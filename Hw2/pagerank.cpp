#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

struct Node
{
	vector<int> linked_node;
	int out_links = 0;
};

vector<Node> graph;
double DAMP = 0.85;
double EPSLN = 0.000001;

int build_graph(char* argv)
{
	int n;
	char node_and_links[50];
	int node_no, out_links;
	FILE* fp = fopen(argv, "r");
	if (fp == NULL)
	{
		fprintf(stderr, "open error\n");
		exit(1);
	}
	
	fscanf(fp, "%*s%d", &n);
	graph.resize(n);

	while (fscanf(fp, "%s", node_and_links) != EOF)
	{
		sscanf(node_and_links, "%d:%d", &node_no, &out_links);
		graph[node_no - 1].out_links = out_links;
		graph[node_no - 1].linked_node.resize(out_links);

		for (int i = 0; i < out_links; i++)
			fscanf(fp, "%d", &graph[node_no - 1].linked_node[i]);
	}

	fclose(fp);
	return n;
}

bool converged(int n, vector<double>& score_prev, vector<double>& score_curr)
{
	double sum_of_square = 0;
	for (int i = 0; i < n; i++)
		sum_of_square += (score_prev[i] - score_curr[i]) * (score_prev[i] - score_curr[i]);

	return (sqrt(sum_of_square) < EPSLN);
}

void pagerank_transition(int n, vector<double>& score_prev, vector<double>& score_curr)
{
	int flag = 0;
	while (not converged(n, score_prev, score_curr))
	{
		if (flag == 1)
		{
			score_prev = score_curr;
			score_curr.assign(n, 0);
		}
		flag = 1;

		double extra_score = 0;
		for (int i = 0; i < n; i++)
		{
			if (graph[i].out_links == 0)
			{
				extra_score += score_prev[i];
				score_curr[i] -= score_prev[i] / (n - 1);
			}
			else
				for (int j = 0; j < graph[i].out_links; j++)
					score_curr[graph[i].linked_node[j] - 1] += score_prev[i] / graph[i].out_links;
		}
		for (int i = 0; i < n; i++)
			score_curr[i] = DAMP * (score_curr[i] + extra_score / (n - 1)) + (1 - DAMP);
	}

	return;
}

int main(int argc, char* argv[])
{
	if (strcmp(argv[1], "default") != 0)
		DAMP = atof(argv[1]);
	if (strcmp(argv[2], "default") != 0)
		EPSLN = atof(argv[2]);

	int n = build_graph(argv[3]);

	vector<double> score_prev(n, 1);
	vector<double> score_curr(n, 0);
	pagerank_transition(n, score_prev, score_curr);

	for (int i = 0; i < n; i++)
		printf("%d:%f\n", i + 1, score_curr[i]);

	return 0;
}
