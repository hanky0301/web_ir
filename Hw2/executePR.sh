#! /usr/bin/env bash

if [ $# -eq 1 ]; then 
	./pagerank default default $1
elif [ $# -eq 3 ]; then
	if [ $1 == "-d" ]; then
		./pagerank $2 default $3
	elif [ $1 == "-e" ]; then
		./pagerank default $2 $3
	elif [ $1 == "-o" ]; then
		./pagerank default default $3 > $2
	fi
elif [ $# -eq 5 ]; then
	if [ $1 == "-d" ] && [ $3 == "-e" ]; then
		./pagerank $2 $4 $5
	elif [ $1 == "-d" ] && [ $3 == "-o" ]; then
		./pagerank $2 default $5 > $4
	elif [ $1 == "-e" ] && [ $3 == "-o" ]; then
		./pagerank default $2 $5 > $4
	fi
elif [ $# -eq 7 ]; then
	./pagerank $2 $4 $7 > $6
fi
