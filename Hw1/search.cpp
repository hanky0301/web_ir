#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <utility>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <algorithm>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>

#include "rapidxml.hpp"

using namespace std;
using namespace rapidxml;

namespace std
{
	template<>
	struct hash< pair<int, int> >
	{
		size_t operator()(const pair<int, int>& p) const
		{
			return (hash<int>()(p.first) ^ (hash<int>()(p.second) << 1));
		}
	};
}

typedef unordered_map<string, int> WORD_TO_ID;
typedef unordered_map<pair<int, int>, double> BIWORD_TO_WEIGHT;
typedef unordered_map<int, BIWORD_TO_WEIGHT> FILENUM_TO_DVEC;

struct Ranking_doc
{
	int file_num;
	double similarity;
};


void build_vocabID_table(char* path, WORD_TO_ID& vocab_id)
{
	char full_path[200];
	strcpy(full_path, path);
	strcat(full_path, "/vocab.all");
	ifstream file(full_path);
	string vocab;
	int id = 0;
	while (getline(file, vocab))
		vocab_id[vocab] = id++;
	
	file.close();
	return;
}

void build_doclen_docID_table(char* file_list_path, char* NTCIR_path, int doclen[],
							  char doc_id[][35], double& avgdoclen, int& total_docs)
{
	char full_path[200];
	strcpy(full_path, file_list_path);
	strcat(full_path, "/file-list");
	NTCIR_path[strlen(NTCIR_path) - 7] = '\0';
	FILE* fp = fopen(full_path, "r");
	char doc_path[35];
	
	int i = 0;
	double total_doclen = 0;
	while (fgets(doc_path, 35, fp) != NULL)
	{
		doc_path[31] = '\0';
		strcpy(doc_id[i], doc_path);

		char doc_full_path[200];
		strcpy(doc_full_path, NTCIR_path);
		strcat(doc_full_path, doc_path);
                               
		FILE* fp_doc = fopen(doc_full_path, "rb");
		fseek (fp_doc, 0, SEEK_END);
		doclen[i] = ftell(fp_doc);
		total_doclen += doclen[i];
		i++;
		fclose(fp_doc);
	}
	avgdoclen = total_doclen / i;
	total_docs = i;
	
	fclose(fp);
	return;
}

void build_query_vectors(char* path, BIWORD_TO_WEIGHT q_vec[], WORD_TO_ID& vocab_id, 
						 int& total_queries, string topic_id[])
{
	ifstream file(path);
	stringstream buffer;
	buffer << file.rdbuf();
	file.close();
	string content = buffer.str();
	xml_document<> doc;
	doc.parse<0>(&content[0]);

	xml_node<>* topic = doc.first_node("xml")->first_node("topic");
	int i = 0;
	while (topic != NULL)
	{
		string number = topic->first_node("number")->value();
		topic_id[i] = number.substr(14, 3);
		string concepts = topic->first_node("concepts")->value();
		concepts.erase(concepts.end() - 4, concepts.end());

 		for (unsigned int j = 1; j < concepts.size() - 3; j += 3)
			if (concepts.substr(j, 6).find("、") == string::npos)
			{
				pair<int, int> p(vocab_id[concepts.substr(j, 3)], 
								 vocab_id[concepts.substr(j + 3, 3)]);
				q_vec[i][p] = 1;
			}

		i++;
		topic = topic->next_sibling("topic");
	}
	
	total_queries = i;
	return;
};

void build_doc_vectors(char* path, BIWORD_TO_WEIGHT q_vec[], FILENUM_TO_DVEC d_vec_map[], 
					   int total_queries, int doclen[], double avgdoclen, int total_docs)
{
	char full_path[200];
	strcpy(full_path, path);
 	strcat(full_path, "/inverted-file");
	FILE* fp = fopen(full_path, "r");
	int id1, id2, n;

	while (fscanf(fp, "%d%d%d", &id1, &id2, &n) != EOF)
	{
		pair<int, int> p(id1, id2);
		int file_num[n];
		int count[n];

		for (int i = 0; i < n; i++)
			fscanf(fp, "%d%d", &file_num[i], &count[i]);

		for (int i = 0; i < total_queries; i++)
		{
			if (q_vec[i].find(p) != q_vec[i].end())
			{
				double idf = log2((0.5 + total_docs - n) / (0.5 + n));
				for (int j = 0; j < n; j++)
				{
					double tf = 2.4 * count[j] / (count[j] + 1.4 * 
								(1 - 0.75 + 0.75 * doclen[file_num[j]] / avgdoclen));

					d_vec_map[i][file_num[j]][p] = tf * idf;
				}
			}
		}
	}
	
	fclose(fp);
	return;
}

bool compare(Ranking_doc& x, Ranking_doc& y)
{
	return x.similarity > y.similarity;
}

void generate_ranking_list(char* path, BIWORD_TO_WEIGHT q_vec[], FILENUM_TO_DVEC d_vec_map[], 
						   int total_queries, char doc_id[][35], string topic_id[], int flag)
{
	for (int i = 0; i < total_queries; i++)
	{
		Ranking_doc doc[d_vec_map[i].size()];
		int j = 0;
		for (auto& d: d_vec_map[i])
		{
			double inner_product = 0;
			for (const auto& coord: q_vec[i])
				inner_product += coord.second * d.second[coord.first];
			
			doc[j].file_num = d.first;
			doc[j].similarity = inner_product;
			j++;
		}
		sort(doc, doc + d_vec_map[i].size(), compare);

		for (int k = 0; k < 100; k++)
		{
			if (flag)
			{
				if (k < 5)
					for (auto& coord: d_vec_map[i][doc[k].file_num])
						q_vec[i][coord.first] += 0.5 / 5 * coord.second;
			}
			else
			{
				int len = strlen(doc_id[doc[k].file_num] + 16);
				for (int t = 0; t < len; t++)
					doc_id[doc[k].file_num][t + 16] = tolower(doc_id[doc[k].file_num][t + 16]);
				cout << topic_id[i];
				printf(" %s\n", doc_id[doc[k].file_num] + 16);
			}
		}
	}
	if (flag)
		generate_ranking_list(path, q_vec, d_vec_map, total_queries, doc_id, topic_id, 0);

	return;
}

int main(int argc, char* argv[])
{
	WORD_TO_ID vocab_id(30000);
	string topic_id[100];
	int total_queries;
	int doclen[47000];
	char doc_id[47000][35];
	double avgdoclen;
	int total_docs;
	BIWORD_TO_WEIGHT q_vec[100];
	FILENUM_TO_DVEC d_vec_map[100];

	int flag = (strcmp(argv[5], "feedback") == 0);

	build_vocabID_table(argv[3], vocab_id);
	build_doclen_docID_table(argv[3], argv[4], doclen, doc_id, avgdoclen, total_docs);
	build_query_vectors(argv[1], q_vec, vocab_id, total_queries, topic_id);
	build_doc_vectors(argv[3], q_vec, d_vec_map, total_queries, doclen, avgdoclen, total_docs);
	generate_ranking_list(argv[2], q_vec, d_vec_map, total_queries, doc_id, topic_id, flag? 1 : 0);

	return 0;
}
